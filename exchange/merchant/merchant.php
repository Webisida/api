<?php
header('Content-Type: text/html; charset=utf-8');
$api = 2;  // id интерфейса
$key = 'sign_key'; // секретный ключ для подписи запросов
$time = gmdate('Y-m-d H:i:s'); // время запроса
$apiUrl = 'http://api.webisida.com';

$payee = $api;
$payer = 1; // пользователь, которому выставляется счет
$amount = 100.01;
$currency = 'Credits';
$note = 'тестовый счет';
$expireIn = 900;
$invId = time(); // id счета в системе учета биржи

$userData = array();
//$userData['myBData'] = 'данные B';
//$userData['myCData'] = 'данные C';
//$userData['myAData'] = 'данные A';

$sigMsg = "$api::$time::$key::$amount::$currency::$expireIn::$invId::$note::$payee::$payer";
if(count($userData) > 0) {
	ksort($userData);
	reset($userData);
	foreach($userData as $k => $v) {
		$sigMsg = $sigMsg.'::'.$v;
	}
}
$sig = md5($sigMsg);

?><!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8" />
		<title></title>
	</head>
	<body>
		<form action="<?=$apiUrl?>/Merchant/Pay" method="post">
			<input type="hidden" name="Api" value="<?=$api?>"/>
			<input type="hidden" name="Timestamp" value="<?=$time?>"/>
			<input type="hidden" name="InvId" value="<?=$invId?>"/>
			<input type="hidden" name="Payee" value="<?=$payee?>"/>
			<input type="hidden" name="Payer" value="<?=$payer?>"/>
			<input type="hidden" name="Amount" value="<?=$amount?>"/>
			<input type="hidden" name="Currency" value="<?=$currency?>"/>
			<input type="hidden" name="ExpirationTimeout" value="<?=$expireIn?>"/>
			<input type="hidden" name="Note" value="<?=htmlentities($note, ENT_COMPAT, 'UTF-8')?>"/>
<?php
	foreach($userData as $k => $v) {
		echo '<input type="hidden" name="UserData['.htmlentities($k, ENT_COMPAT, 'UTF-8').']" value="'.htmlentities($v, ENT_COMPAT, 'UTF-8').'"/>';
	}	
?>
			<input type="hidden" name="Sig" value="<?=$sig?>"/>
			<input type="submit" value="Оплатить"/>
		</form>
	</body>
</html>

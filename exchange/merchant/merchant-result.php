<?php
header('Content-Type: application/json; charset=utf-8');

function getParam($name) {
	return isset($_POST[$name]) ? $_POST[$name] : null;
}

function getCustomParam($name) {
	return isset($_POST['userData'][$name]) ? $_POST['userData'][$name] : null;
}

$api = getParam('api');
$timestamp = getParam('timestamp');
$method = getParam('method');
$invId = getParam('invId');
$payeeTransactionId = getParam('payeeTransactionId');
$amount = getParam('amount');
$currency = getParam('currency');
$note = getParam('note');
$payee = getParam('payee');
$payer = getParam('payer');
$sig = getParam('sig');
$userData = getParam('userData');
$key = 'notify_sign_key';

$sigMsg = implode('::', array(
	$api,
	$timestamp,
	$key,
	$amount,
	$currency,
	$invId,
	$method,
	$note,
	$payee,
	$payeeTransactionId,
	$payer
));

if(is_array($userData)) {
	ksort($userData);
	reset($userData);
	foreach($userData as $k => $v) {
		$sigMsg = $sigMsg.'::'.$v;
	}
}

$sign = md5($sigMsg);

if($sig != $sign) {
	file_put_contents('bad-sign.txt', $sig."\r\n".$sign);
	$data = array(
		'error' => array(
			'code' =>  -32000,
			'message' => 'неверная подпись'
		)
	);
}
else
{
	file_put_contents($method.'.txt', $method.' '.$timestamp);
	$data = array(
		'result' => array(
			'message' => 'OK'
		)
	);
}
echo json_encode($data, 256 /* JSON_UNESCAPED_UNICODE */);

<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta charset="utf-8"/>
	</head>
	<body>
<?php

$amount = isset($_GET['amount']) ? floatval($_GET['amount']) : 0;
$invid = isset($_GET['invId']) ? intval($_GET['invId']) : 0;
$errcode  = isset($_GET['errcode']) ? $_GET['errcode'] : 2;

echo "Счет № $invid на сумму $amount не был оплачен: ";

switch($errcode) {
	case 1: case 'Reject':
		echo 'счет отклонен';
		break;
	case 2: case 'Generic':
		echo 'неизвестная ошибка';
		break;
	case 3: case 'InactiveApi':
		echo 'API или мерчант не активен';
		break;
	case 4: case 'InvalidTimestamp':
		echo 'недопустимый TimeStamp';
		break;
	case 5: case 'InvalidSignature':
		echo 'неверная подпись';
		break;
	case 6: case 'DuplicateInvId':
		echo 'счет с таким же ID уже создан';
		break;
	case 7: case 'InvalidPayee':
		echo 'пользователь не существует';
		break;
	case 8: case 'InvalidPayer':
		echo 'пользователь не существует';
		break;
	case 9: case 'InvalidAmount':
		echo 'недопустимая сумма';
		break;
	case 10: case 'InvalidCurrency':
		echo 'недопустимая валюта';
		break;
	case 11: case 'InvalidExpiration':
		echo 'недопустимое время жизни счета';
		break;
	case 12: case 'TooLongNote':
		echo 'описание счета длиннее 1000 символов';
		break;
	case 12: case 'Expired':
		echo 'срок оплаты счета истек';
		break;
	default:
		echo 'ошибка';
		break;
}
?>
	</body>
</html>
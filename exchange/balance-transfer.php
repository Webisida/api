<?php

$api = 2; // id интерфейса
$key = 'sign_key'; // ключ для подписи запросов
$time = gmdate('Y-m-d H:i:s');
$apiUrl = 'http://api.webisida.com/v1.0';

if(isset($_POST['submit']))
{
	$payee = 1; // id получателя средств
	$amount = '100.00';
	$currency = 'Credits';
	$payer = $api;
	$note = 'Тестовый перевод';
	$data = "Api=$api&Timestamp=".urlencode($time)."&Payer=$payer&Payee=$payee&Currency=$currency&Amount=$amount&Note=".urlencode($note);
	$data .= '&Sig='.md5("$api::$time::$key::$amount::$currency::$note::$payee::$payer");


	if( $curl = curl_init() ) {
		curl_setopt($curl, CURLOPT_URL, $apiUrl.'/Balance/Transfer');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		$out = curl_exec($curl);
		echo $out;
		curl_close($curl);
	}
}
?><!DOCTYPE html>
<html>
	<head>
		<title></title>
		<meta charset="utf-8"/>
	</head>
	<body>
		<form action="" method="post">
			<input type="submit" name="submit" value="Сделать перевод"/>
		</form>
	</body>
</html>
